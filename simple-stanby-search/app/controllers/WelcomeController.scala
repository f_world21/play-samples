package controllers

import play.api.mvc._

/**
 * Created by s.kobayashi on 10/8/15.
 */
class WelcomeController extends Controller {
  def index = Action { request =>
    Ok(views.html.welcome.index.render("すごいよね"))
  }

  def hoge = Action { request =>
    Ok("GREAT DAZE!!")

  }
}
