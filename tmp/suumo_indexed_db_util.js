(function(){var global=this;var indexedDB=global.indexedDB||global.webkitIndexedDB||global.mozIndexedDB;if(!indexedDB){return;}
var INDEDED_DB_NAME='smp-suumo-jp-db';var INDEXED_DB_STORE_LIST=[{name:'shareInfoWithSW',keyPath:'useName'}];var db=null;var openRequest=indexedDB.open(INDEDED_DB_NAME,1);openRequest.onsuccess=function(event){db=event.target.result;};openRequest.onupgradeneeded=function(event){var db=event.target.result;for(var i=0;i<INDEXED_DB_STORE_LIST.length;i++){var storeInfo=INDEXED_DB_STORE_LIST[i];db.createObjectStore(storeInfo.name,{keyPath:storeInfo.keyPath});}};function addToStore(storeName,data,onsuccess,onerror){if(!db){if(typeof onerror==='function'){onerror();}
return;}
var store=db.transaction([storeName],'readwrite').objectStore(storeName);var addRequest=store.add(data);addRequest.onsuccess=onsuccess;addRequest.onerror=onerror;}
function updateToStore(storeName,keyPathString,data,isAdd,onsuccess,onerror){if(!db){if(typeof onerror==='function'){onerror();}
return;}
var store=db.transaction([storeName],'readwrite').objectStore(storeName);var getRequest=store.get(keyPathString);getRequest.onsuccess=function(event){var putRequest=store.put(data);putRequest.onsuccess=onsuccess;putRequest.onerror=onerror;};if(isAdd){getRequest.onerror=function(event){var addRequest=store.add(data);addRequest.onsuccess=onsuccess;addRequest.onerror=onerror;}}
else{getRequest.onerror=onerror;}}
function getFromStore(storeName,keyPathString,onsuccess,onerror){if(!db){if(typeof onerror==='function'){onerror();}
return;}
var store=db.transaction([storeName],'readwrite').objectStore(storeName);var getRequest=store.get(keyPathString);getRequest.onsuccess=onsuccess;getRequest.onerror=onerror;}
function deleteFromStore(storeName,keyPathString,onsuccess,onerror){if(!db){if(typeof onerror==='function'){onerror();}
return;}
var store=db.transaction([storeName],'readwrite').objectStore(storeName);var deleteRequest=store.delete(keyPathString);deleteRequest.onsuccess=onsuccess;deleteRequest.onerror=onerror;}
var indexedDBUtil={addToStore:addToStore,updateToStore:updateToStore,getFromStore:getFromStore,deleteFromStore:deleteFromStore,INDEXED_DB_STORE_LIST:INDEXED_DB_STORE_LIST};global.indexedDBUtil=indexedDBUtil;}());